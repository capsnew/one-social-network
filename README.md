# Introduction
This project doesn't have a name yet, so we'll name it "one social network" (or OSN) until we find one.
# Proposition
OSN is defined by 2 functions: you can be related to only 10 other users and you can do only one post per day (1024 signs).
# Where am I?
I want to add one record in each table at least, in order to work properly.  
But here is my problem: I want to store passwords safely, what's normal, so I'm looking for a function in SQL to do it (probably hash them, I'm not a master in security of data).  
Then, this part will be done in PHP/SQL, thanks to PDO.  
When I'll found that, I begin with the pages of connexion and create an account. For now, I think to code OSN with PHP, PDO, HTML, MySQL, Bootstrap. We'll be able to add features in JavaScript, JQuery and AngularJS later, even write our own UI in CSS without using Bootstrap.  
EDIT: I'm on another project at the same time, which is more important that this one. It has features in common with OSN that I'll be able to use for it. So I'm working on both but progress on OSN won't be uploaded soon.
# Documentation links
[Syntax to create a table](https://dev.mysql.com/doc/refman/5.7/en/create-table.html)  
[MySQL encryption functions](https://dev.mysql.com/doc/refman/5.5/en/encryption-functions.html)  
[PDO::lastInsertId](https://secure.php.net/manual/en/pdo.lastinsertid.php)  
