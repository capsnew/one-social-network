INSERT INTO diffusion(idDiffusion, nameDiffusion) VALUES (0, AES_ENCRYPT("Private", "osn"));    /* Visible by only me. */
INSERT INTO diffusion(idDiffusion, nameDiffusion) VALUES (1, AES_ENCRYPT("Protected", "osn"));  /* Visible by only my relations. */
INSERT INTO diffusion(idDiffusion, nameDiffusion) VALUES (2, AES_ENCRYPT("Public", "osn"));     /* Visible by everyone. */
