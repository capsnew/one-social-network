DROP DATABASE IF EXISTS osn;
CREATE DATABASE IF NOT EXISTS osn CHARACTER SET UTF8;

USE osn;
DROP TABLE IF EXISTS person;
DROP TABLE IF EXISTS diffusion;
DROP TABLE IF EXISTS post;
DROP TABLE IF EXISTS relation;

CREATE TABLE IF NOT EXISTS person(
    idPerson        INT UNSIGNED    NOT NULL    AUTO_INCREMENT  UNIQUE  PRIMARY KEY
,   pseudoPerson    LONGBLOB        NOT NULL                    UNIQUE
,   pwdPerson       LONGBLOB        NOT NULL
,   fnamePerson     LONGBLOB
,   lnamePerson     LONGBLOB
,   secondPseudo    LONGBLOB
,   pwdHint         LONGBLOB
) CHARACTER SET UTF8;

CREATE TABLE IF NOT EXISTS diffusion(
    idDiffusion     INT UNSIGNED                                                                                        NOT NULL    UNIQUE  PRIMARY KEY
,   nameDiffusion   SET(AES_ENCRYPT("Private", "osn"), AES_ENCRYPT("Protected", "osn"), AES_ENCRYPT("Public", "osn"))   NOT NULL) CHARACTER SET utf8;

CREATE TABLE IF NOT EXISTS post(
    idPost      INT UNSIGNED    NOT NULL    AUTO_INCREMENT  UNIQUE  PRIMARY KEY
,   idPerson    INT UNSIGNED    NOT NULL                            REFERENCES person(idPerson)
,   idDiffusion INT UNSIGNED    NOT NULL    DEFAULT 0
,   contentPost LONGBLOB) CHARACTER SET utf8;

CREATE TABLE IF NOT EXISTS relation(
    idRelation  INT UNSIGNED    NOT NULL    AUTO_INCREMENT              UNIQUE  PRIMARY KEY
,   idPerson1   INT UNSIGNED    NOT NULL                                        REFERENCES person(idPerson)
,   idPerson2   INT UNSIGNED    NOT NULL                                        REFERENCES person(idPerson)) CHARACTER SET utf8;